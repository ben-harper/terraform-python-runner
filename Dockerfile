FROM python:3

LABEL maintainer="ben.harper@pwc.com"

ENV TERRAFORM_VERSION="0.14.5"

RUN apt-get update

RUN curl https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip >terraform_${TERRAFORM_VERSION}_linux_amd64.zip && \
  unzip terraform_${TERRAFORM_VERSION}_linux_amd64.zip -d /bin && \
  rm -rf terraform_${TERRAFORM_VERSION}_linux_amd64.zip
